<?php
/**
 * Copyright © 2016 DSTax. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'DSTax_TaxConnect',
    __DIR__
);