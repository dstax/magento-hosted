""" Script to configure and package the DS Tax taxconnect Magento 2 extension
for a specific customer. """
import sys
import os.path
from subprocess import run, PIPE
import time

def os_call(arg_list, cwd=None, stdin=None):
    # print and run a command using subprocess call
    for arg in arg_list:
        print(arg + " ", end="")
    print("\n")
    result = run(arg_list, cwd=cwd, stdin=stdin, stdout=PIPE,
                 stderr=PIPE)
    if result.returncode != 0:
        # print stdout and stderr
        print("ERROR:")
        print(result.stdout.decode("utf-8"))
        print(result.stderr.decode("utf-8"))


if sys.version_info[0] < 3:
    print("This script requires Python 3")
else:
    if len(sys.argv) == 2:
        git_repo = sys.argv[1]
    else:
        print("Usage: " + sys.argv[0] + " <path/to/git/repo/>")
        sys.exit()
    # get code using git archive to avoid including unneccessary files
    if os.path.exists("packages"):
        print("packages directory already exists")
    else:
        os_call(["mkdir", "packages"])
    repo_dir = git_repo.replace(".git", "")
    assert os.path.isdir(git_repo), "Git repo not found!"
    tar_file = "TaxConnect.tar"
    os_call(["git", "archive", "--prefix", "TaxConnect/", "-o", tar_file, "master"], cwd=repo_dir)

    # generate .tgz package
    os_call(["gzip", tar_file])
    targz_file = tar_file + ".gz"
    date_time_string = "-" + time.strftime("%Y-%m-%d-%H%M%S")
    os_call(["mv", targz_file, "packages/TaxConnect" + date_time_string + ".tgz"])
    
    zip_file = "TaxConnect.zip"
    zip2_file = "DSTaxTaxConnect.zip"
    folder_name = "TaxConnect"
    
    os_call(["git", "archive", "--prefix", "TaxConnect/", "-o", zip_file, "master"], cwd=repo_dir)
    os_call(["git", "archive", "--prefix", "DSTax/TaxConnect/", "-o", zip2_file, "master"], cwd=repo_dir)
    os_call(["mv", zip_file, "packages/TaxConnect" + date_time_string + ".zip"])
    os_call(["mv", zip2_file, "packages/DSTaxTaxConnect" + date_time_string + ".zip"])
