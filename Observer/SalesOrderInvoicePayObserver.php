<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace DSTax\TaxConnect\Observer;

use Magento\Framework\Event\ObserverInterface;

use DSTax\TaxConnect\Model\Sales\Total\Quote\Tax;
// use Magento\Sales\Model\Order;
use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;

/**
 * Class SalesOrderInvoicePayObserver
 */
class SalesOrderInvoicePayObserver implements ObserverInterface
{
    /**
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    protected $logger;
    protected $scopeConfig;
    protected $taxConfig;
    protected $taxData;
    protected $taxCalculationInterface;
    protected $quoteDetailsDataObjectFactory;
    protected $quoteDetailsItemDataObjectFactory;
    protected $taxClassKeyDataObjectFactory;
    protected $customerAddressFactory;
    protected $customerAddressRegionFactory;
    
    /**
     * Get everything required for the Tax class constructor.
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory
     * @param \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory
     * @param CustomerAddressFactory $customerAddressFactory
     * @param CustomerAddressRegionFactory $customerAddressRegionFactory
     * @param \Magento\Tax\Helper\Data $taxData
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationInterface,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->taxConfig = $taxConfig;
        $this->taxCalculationInterface = $taxCalculationInterface;
        $this->quoteDetailsDataObjectFactory = $quoteDetailsDataObjectFactory;
        $this->quoteDetailsItemDataObjectFactory = $quoteDetailsItemDataObjectFactory;
        $this->taxClassKeyDataObjectFactory = $taxClassKeyDataObjectFactory;
        $this->customerAddressFactory = $customerAddressFactory;
        $this->customerAddressRegionFactory = $customerAddressRegionFactory;
        $this->taxData = $taxData;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Make a tax call if PostToAudit is true.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $tax = new Tax(
            $this->taxConfig,
            $this->taxCalculationInterface,
            $this->quoteDetailsDataObjectFactory,
            $this->quoteDetailsItemDataObjectFactory,
            $this->taxClassKeyDataObjectFactory,
            $this->customerAddressFactory,
            $this->customerAddressRegionFactory,
            $this->taxData,
            $this->logger,
            $this->scopeConfig
        );
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        //$tax->getConfigValue('OSITD_PostToAudit', $storeScope);
        $auditInvoice = $tax->getConfigValue('auditInvoices', $storeScope);

        // FIND MAGENTO ROOT DIRECTORY FOR LOGGING
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $logDirectory = $directory->getRoot() . '/var/log/';
        $file = $logDirectory . 'taxConnect.log';

        $invoice = $observer->getEvent()->getData('invoice');
        $order = $invoice->getOrder();
        // Prepare arguments for call to tax calculation API
        $customer = array(
            'ID' => $order->getCustomerId(),
            'Name' => $order->getCustomerName(),
            'VAT' => $order->getCustomerTaxvat(),
        );
        $shipping_address = $order->getShippingAddress();
        $billing_address = $order->getBillingAddress();
        if (!$billing_address) {
            $billing_address = $shipping_address;
        }
        $order_info = array(
            'ID' => $order->getRealOrderId(),
            'billing_address' => $billing_address,
            'items' => $order->getAllItems(),
            'shipping_address' => $shipping_address,
            'total_subtotal' => $order->getSubtotal(),
            'total_shipping' => $order->getShippingAmount(),
            'external_company_id' => $tax->getConfigValue('ExternalCompanyId', $storeScope),
        );

        // Log content
        $logs = " This is from SalesOrderInvoicePayObserver";
        $logs .= json_encode($order_info, JSON_PRETTY_PRINT);
        $logs .= json_encode($customer, JSON_PRETTY_PRINT);
        file_put_contents($file, $logs, FILE_APPEND);

        // $tax->getTaxFromOneSource($customer, $order_info, $audit);
        if($auditInvoice == 1){
            $tax->getTaxFromOneSource($customer, $order_info, 'false', 'true');
        }
        return $this;
    }
}
