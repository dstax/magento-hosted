# TaxConnect for Magento 2

The DS Tax TaxConnect extension for Magento 2 allows you to use Thomson Reuters ONESOURCE Indirect Tax Determination Service to calculate transaction taxes for your Magento Store.

## Installing TaxConnect

Download the taxconnect-xxx-xxx.tgz file from this repository.

To install TaxConnect in your system you need to place the entire zip package provided under the “code” directory(/var/www/html/app/code), This will create the following path (/app/code/DSTax/TaxConnect)with the integration inside. After the code is placed there, run the following command: 

$ bin/magento module:status 

This will display the entire list of enabled and disabled modules. You should now see DSTax_TaxConnect in this list under List of disabled modules. As the description implies, Magento now recognizes the module but it is not yet active.  To activate the module run the following command: 

$ bin/magento module:enable DSTax_TaxConnect 

At this point, if you run the module:status command again you should see DSTax_TaxConnect under the list of enabled modules.

You should also verify that the module was successfully installed by going to the Magento admin, clicking on SYSTEM, then Web Setup Wizard. This will take you to the Setup Wizard page where you can click on component manager to view installed modules.

You should have received an API Key in an email from ITBridge@dstax.com.

To enable tax calculations, select Stores->Configuration->Sales->TR OSITD in the Magento Admin Panel, and paste your API key into the field as shown here:

![](images/api-key-screen-shot.png)

Click Save Config.  Magento installation is now complete, please login into ITBridge.dstax.com to complete your configuration.


Event name: sales_order_payment_refund
File: vendor/magento/module-sales/Model/Order/Payment.php

	$this->_eventManager->dispatch(
		'sales_order_payment_refund',
		['payment' => $this, 'creditmemo' => $creditmemo]
	);

gotta remove old branch