<?php
/**
 * Copyright: DSTax, LLC, 2020
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions.  You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Config\Source;

/**
 * These are the options for the Company Role drop down menu in the admin UI.
 */
class Environment implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Retrieve list of options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'https://test.itbridge.dstax.com/api/v0/tax/',
                'label' => __('Testing - ITBridge')
            ],
            [
                'value' => 'https://uat.itbridge.dstax.com/api/v0/tax/',
                'label' => __('User Acceptance Testing - ITBridge')
            ],
            [
                'value' => 'https://nexus.itbridge.dstax.com/api/v0/tax/',
                'label' => __('Nexus Testing')
            ],
            [
                'value' => 'https://itbridge.dstax.com/api/v0/tax/',
                'label' => __('Production - ITBridge')
            ],
            [
                'value' => 'https://nexus.itbridge.dstax.com/api/v0/tax/',
                'label' => __('Nexus Testing - ITBridge')
            ],
            [
                'value' => 'https://uat.portal.dstax.com/api/v0/tax/',
                'label' => __('User Acceptance Testing - DevPortalv0')
            ],
            [
                'value' => 'https://portal.dstax.com/api/v0/tax/',
                'label' => __('Production - DevPortalv0')
            ],
            [
                'value' => 'https://test.portal.dstax.com/api/v0/tax/',
                'label' => __('Test - DevPortalv0')
            ]
        ];
    }
}

