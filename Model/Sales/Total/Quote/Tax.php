<?php
/**
 * Copyright: DSTax, LLC, 2018
 * The DSTAX SOFTWARE is protected by copyright laws and international treaty
 * provisions. You may not reverse engineer, decompile, or disassemble the
 * DSTAX SOFTWARE.
 **/
namespace DSTax\TaxConnect\Model\Sales\Total\Quote;

use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address;

use Magento\Tax\Api\Data\TaxClassKeyInterface;
use Magento\Tax\Model\Calculation;

/**
 * Tax totals calculation model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Tax extends \Magento\Tax\Model\Sales\Total\Quote\Tax
{
    const AVS_ENABLED = false;            // Address validation service
    const ALLOCATE_SHIPPING = true;       // make this user configurable
    const ALLOCATE_DISCOUNT = true;       // make this user configurable
    const BUNDLE_PRICE_ZERO = false;      // make this user configurable
    const SAP_CUSTOMER_ID = false;        // make this user configurable
    const TIVO_BUNDLE_ATTRIBUTE = false;  // make this user configurable
    const USE_COMMODITY_CODE = false;     // pass tax class to commodity code
    const SAP_CREDIT_AUTH = false;        // for credit card authorization
    const EXCLUDE_FROM_OSITD = false;     // exclude some items from the tax calc
    const ERP_CUST_NUM = false;           // pass ERP cust num to partner num
    const DIGICERT_CUSTOMER = true;      // Digicert Customer custom fields

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory
     */
    protected $extensionFactory;
    protected $logger;
    protected $scopeConfig;
    protected $logDirectory;
    protected $addressRepositoryInterface;
    protected $customerRepositoryInterface;
    protected $session;

    /**
     * @var CacheInterface $cache
     */
    private CacheInterface $cache;

    /**
     *
     */
    public function writeToLog($logs) {
        $file = $this->logDirectory . 'taxConnect.log';
        file_put_contents($file, $logs, FILE_APPEND);
    }

    /**
     * Class constructor
     *
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService
     * @param \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory
     * @param \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory
     * @param CustomerAddressFactory $customerAddressFactory
     * @param CustomerAddressRegionFactory $customerAddressRegionFactory
     * @param \Magento\Tax\Helper\Data $taxData
    // CUSTOM PARAMS BELOW
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
     * @param \Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory
     * @param CacheInterface $cache
     */
    public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
        // CUSTOM CLASSES BELOW
        //\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        //\Magento\Tax\Api\Data\QuoteDetailsItemExtensionFactory $extensionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        CacheInterface $cache
    ) {
        // CUSTOM DEFINITIONS
        //$this->priceCurrency    = $priceCurrency;
        //$this->extensionFactory = $extensionFactory;
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->cache = $cache;

        // FIND MAGENTO ROOT DIRECTORY FOR LOGGING
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $directory = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->logDirectory = $directory->getRoot() . '/var/log/';

        parent::__construct(
            $taxConfig,
            $taxCalculationService,
            $quoteDetailsDataObjectFactory,
            $quoteDetailsItemDataObjectFactory,
            $taxClassKeyDataObjectFactory,
            $customerAddressFactory,
            $customerAddressRegionFactory,
            $taxData
        );
    }


    /**
     * Collect tax totals for quote address
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $this->clearValues($total);
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $address = $shippingAssignment->getShipping()->getAddress();
        $hasValidShippingAddress = true;
        // Do not make a tax call for US and Canadian addresses without a postal code
        if (($address->getCountry() == 'US' || $address->getCountry() == 'CA')
            && !$address->getPostcode()
        ) {
            $hasValidShippingAddress = false;
        }
        if ($shippingAssignment->getItems() && $hasValidShippingAddress) {
            $baseQuoteTaxDetails = $this
                ->getQuoteTaxDetailsInterface($shippingAssignment, $total, true);
            $baseTaxDetails = $this
                ->getQuoteTaxDetails($shippingAssignment, $total, true);
            $taxDetails = $this
                ->getQuoteTaxDetails($shippingAssignment, $total, false);

            //Populate address and items with tax calculation results
            $itemsByType = $this
                ->organizeItemTaxDetailsByType($taxDetails, $baseTaxDetails);
            if (isset($itemsByType[self::ITEM_TYPE_PRODUCT])) {
                $this->processProductItems(
                    $shippingAssignment,
                    $itemsByType[self::ITEM_TYPE_PRODUCT],
                    $total
                );
            }

            if (isset($itemsByType[self::ITEM_TYPE_SHIPPING])) {
                $shippingTaxDetails
                    = $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_ITEM];
                $baseShippingTaxDetails
                    = $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_BASE_ITEM];
                $this->processShippingTaxInfo(
                    $shippingAssignment,
                    $total,
                    $shippingTaxDetails,
                    $baseShippingTaxDetails
                );
            }

            // Prepare arguments for call to tax calculation API
            $customer = array(
                'ID' => $quote->getCustomerId(),
                'Name' => $quote->getCustomerFirstname().' '.$quote->getCustomerLastname(),
                // 'VAT' => $quote->getCustomerTaxClassId(),
                'VAT' => $quote->getCustomerTaxvat(),
            );
            $order = array(
                'ID' => $quote->getId(),
                'billing_address' => $quote->getBillingAddress(),
                'items' => $shippingAssignment->getItems(),
                'shipping_address' => $shippingAssignment
                    ->getShipping()->getAddress(),
                'total_subtotal' => $total->getTotalAmount('subtotal'),
                'total_shipping' => $total->getTotalAmount('shipping'),
                'external_company_id' => $this->getConfigValue('ExternalCompanyId', $storeScope),
            );

            if (!in_array($this->getConfigValue('Active', $storeScope), ["", "false", "0"])) {
                $this->updateTax(
                    $this->getTaxFromOneSource($customer, $order),
                    $shippingAssignment,
                    $total
                );
                $this->writeToLog("\n\n TAX IS CALLED \n\n");
            } else {
                $this->writeToLog("\n\n NOOOOOOOOOOO TAX CALL\n\n");
            }

            //Process taxable items that are not product or shipping
            $this->processExtraTaxables($total, $itemsByType);

            //Save applied taxes for each item and the quote in aggregation
            $this->processAppliedTaxes($total, $shippingAssignment, $itemsByType);
            if ($this->includeExtraTax()) {
                $total->addTotalAmount('extra_tax', $total->getExtraTaxAmount());
                $total->addBaseTotalAmount(
                    'extra_tax', $total->getBaseExtraTaxAmount()
                );
            }
        }
        return $this;
    }

    /**
     * Get tax from OneSource
     *
     * @param Array  $customer customer info required for tax call
     * @param Array  $order    order info required for tax call
     * @param string $audit    PostToAudit flag for tax call
     *
     * @return string
     */
    public function getTaxFromOneSource($customer, $order, $audit = 'false', $fromInvoice = 'false')
    {
        $shippingAddress = $order['shipping_address'];
        $logMessage = (
            "\n\n CUSTOMER ORDER DATA \n\n" .
            "\n\nOrder: " .
            json_encode($order, JSON_PRETTY_PRINT) .
            "\n\nCustomer: " .
            json_encode($customer, JSON_PRETTY_PRINT) .
            "\n\nAudit: " .
            $audit
        );
        $this->writeToLog($logMessage);
        if ($shippingAddress && !$shippingAddress->getPostcode()) {
            return;
        }
        // Do not make a tax call if the cart is empty
        if (!count($order['items'])) {
            return;
        }
        // Get API Key
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $api_key = $this->getConfigValue("DSTAX_APIKey", $storeScope);

        /*
         * Create an associative array for the bill to address.
         */
        $BillToAddress = [];
        $billingAddress = $order['billing_address'];
        if ($billingAddress->getCountry() !== null
            AND $billingAddress->getCity() !== null
            AND $billingAddress->getPostcode() !== null
        ) {
            $BillToAddress = [
                'Country' => $billingAddress->getCountry(),
                'Region' => $billingAddress->getRegionCode(),
                'City' => $billingAddress->getCity(),
                'PostalCode' => $this->getZipcode(
                    $billingAddress->getCountry(),
                    $billingAddress->getPostcode()
                ),
                'Country Code' => $billingAddress->getData('country_id'),
            ];
            $billToGeo = $this->getZipExtension(
                $billingAddress->getCountry(),
                $billingAddress->getPostcode()
            );
            if ($billToGeo !== null) {
                $BillToAddress['GeoCode'] = $billToGeo;
            }
        }
        /*
         * Create an associative array for the ship to address.
         */
        $ShipToAddress = [];
        if ($order['shipping_address']) {
            $ShipToAddress = [
                'Address1' => $order['shipping_address']->getStreetFull(),
                'City' => $order['shipping_address']->getCity(),
                'Country' => $order['shipping_address']->getCountry(),
                'GeoCode' => $this
                    ->getZipExtension(
                        $order['shipping_address']->getCountry(),
                        $order['shipping_address']->getPostcode()
                    ),
                'PartnerName' => $customer['Name'],
                'PartnerNumber' => $customer['ID'],
                'PostalCode' => $this
                    ->getZipcode(
                        $order['shipping_address']->getCountry(),
                        $order['shipping_address']->getPostcode()
                    ),
                'Region' => $order['shipping_address']->getRegionCode(),
            ];
            // Get ERP customer number, if applicable
            if (Tax::ERP_CUST_NUM) {
                $ErpCustNum = $this->session->getCustomer()->getErpCustId();
                if ($ErpCustNum) {
                    $ShipToAddress['PartnerNumber'] = $ErpCustNum;
                }
            } else {
                $ShipToAddress['PartnerNumber'] = $customer['ID'];
            }
        }

        /*
         * Create an array of the items in the cart.
         */
        $soap_items = [];
        $tax_items = [];
        foreach ($order['items'] as $item) {
            $tax_items[] = $item;
            $tax_id = $item->getTaxClassId();
            $product_code = $this->getProductCode($item->getTaxClassId());
            if (!$product_code) {
                $product_code = $this->getProductCode($item->getProduct()->getTaxClassId());
            }
            $quantity = $item->getQty();
            if (is_null($quantity)) {
                $quantity = $item->getQtyOrdered();
            }
            $soap_item = [
                'discount'        => (float) $item->getDiscountAmount(),
                'in_bundle'       => false,
                'product_code'    => $product_code,
                'quantity'        => $quantity,
                'sku'             => $item->getSku(),
                'unit_price'      => (float) $item->getPrice(),
            ];
            // Don't double tax dynamically priced bundles
            if (Tax::BUNDLE_PRICE_ZERO) {
                // Set price of bundle to zero for Tivo
                if ($item->getProductType() == 'bundle') {
                    $soap_item['unit_price'] = (float) 0;
                }
            } else {
                // Set prices of items in bundle to zero, default behavior
                $parent = $item->getParentItem();
                if ($parent && $parent->getProductType() == 'bundle') {
                    $soap_item['unit_price'] = (float) 0;
                }
            }
            // Adjust quantity for items in a bundle
            $parent = $item->getParentItem();
            if ($parent && $parent->getProductType() == 'bundle') {
                $soap_item['in_bundle'] = true;
                $soap_item['quantity'] = $parent->getQty() * $quantity;
            } else {
                $soap_item['quantity'] = $quantity;
            }
            $soap_item['user_attributes'] = $this->userAttributes($soap_item);

            $soap_items[] = $soap_item;
        }
        // add line for freight if shipping is not allocated to each line item
        if (!Tax::ALLOCATE_SHIPPING) {
            $soap_item = [
                'discount'         => 0,
                'in_bundle'        => false,
                'line_number'      => 10**5,
                'product_code'     => $this->getConfigValue('ProductCode_Shipping', $storeScope),
                'quantity'         => 1,
                'sku'              => 'FREIGHT',
                'unit_price'       => $order['total_shipping'],
            ];
            $soap_item['user_attributes'] = $this->userAttributes($soap_item);
            $soap_items[] = $soap_item;
        }
        // add line for discounts if not allocated to each line item
        if (!Tax::ALLOCATE_DISCOUNT) {
            $total_discount = 0;
            foreach ($order['items'] as $item) {
                $total_discount += (float) -$item->getDiscountAmount();
            }
            if (abs($total_discount) > 0) {
                $soap_item = [
                    'discount'     => 0,
                    'in_bundle'    => false,
                    'line_number'  => 10**5 + 1,
                    'product_code' => $this->getConfigValue('ProductCode_Discount', $storeScope),
                    'quantity'     => 1,
                    'sku'          => 'DISCOUNT',
                    'unit_price'   => $total_discount,
                ];
                $soap_item['user_attributes'] = $this->userAttributes($soap_item);
                $soap_items[] = $soap_item;
            }
        }
        /*
         * Create invoice attributes.
         */
        $userAttributes = [];
        if (Tax::SAP_CUSTOMER_ID) {
            if ($this->customerRepositoryInterface) {
                //load customer
                $customer = $this->customerRepositoryInterface
                    ->getById($quote->getCustomerId());
                //get customer's default billing address
                $customerBillingAddress = $this->addressRepositoryInterface
                    ->getById($customer->getDefaultBilling());
                //get default billing address sap customer ID
                $billingAddressSapCustomerId = $customerBillingAddress
                    ->getCustomAttribute('sap_customer_id')->getValue();
                $userAttributes['10'] = $billingAddressSapCustomerId;
            }
        }
        if (Tax::DIGICERT_CUSTOMER) {
            if ($customer['ID']) {
                $customer_id = $customer['ID'];
                $field_names = ['dc_account_id', 'dc_container_id', 'dc_tax_external_company_id'];
                $customer_data = $this->getCustomerCustomFieldsValues($customer_id, $field_names);
                if (array_key_exists('dc_account_id', $customer_data)) {
                    $userAttributes['1'] = $customer_data['dc_account_id'];
                }
                if (array_key_exists('dc_container_id', $customer_data)) {
                    $userAttributes['2'] = $customer_data['dc_container_id'];
                }
                if (array_key_exists('dc_tax_external_company_id', $customer_data)) {
                    $userAttributes['3'] = $customer_data['dc_tax_external_company_id'];
                }
                if (
                    array_key_exists('dc_account_id', $customer_data) &&
                    array_key_exists('dc_tax_external_company_id', $customer_data)
                ) {
                    $userAttributes['4'] = $customer_data['dc_account_id'] . 'x' . $customer_data['dc_tax_external_company_id'];
                }
                $logMessage = (
                    "userAttributes: " .
                    json_encode($userAttributes, JSON_PRETTY_PRINT) .
                    "\n\n"
                );
                $this->writeToLog($logMessage);
            }
        }
        // Prepare data for tax call
        $customer_vat = '';
        if (!is_null($customer['VAT'])) {
            $customer_vat = $customer['VAT'];
        }
        if (!$BillToAddress) {
            $BillToAddress = (object) $BillToAddress;
        }
        if (!$ShipToAddress) {
            $ShipToAddress = (object) $ShipToAddress;
        }
        $tax_data = array(
            'addresses' => [
                'BillToAddress' => $BillToAddress,
                'ShipToAddress' => $ShipToAddress,
            ],
            'allocate_discount' => Tax::ALLOCATE_DISCOUNT,
            'allocate_shipping' => Tax::ALLOCATE_SHIPPING,
            'customer_vat' => $customer_vat,
            'items' => $soap_items,
            'order_number' => $order['ID'],
            'total_shipping' => $order['total_shipping'],
            'total_subtotal' => $order['total_subtotal'],
            'use_commodity_code' => Tax::USE_COMMODITY_CODE,
            'user_attributes' => $userAttributes,
            'external_company_id' => $order['external_company_id'],
            'post_to_audit' => 'false',
        );
        // NEW: Add audit flag to tax call
        if($fromInvoice == 'true') {
            if($this->getConfigValue('auditInvoices', $storeScope) == 1) {
                $tax_data['post_to_audit'] = 'true';
            }
        }

        /*
         * Use Laminas HTTP Client to request tax calculation
         */

        $httpHeaders = new \Laminas\Http\Headers();
        $httpHeaders->addHeaders(
            [
                'Authorization' => 'Bearer ' . $api_key,
                'Accept' => 'application/json',
                // 'Accept' => 'application/xml',
                'Content-Type' => 'application/json'
            ]
        );
        (string)$url = $this->getConfigValue('ITBridge_URL', $storeScope);

        $request = new \Laminas\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setUri($url);
        $request->setMethod(\Laminas\Http\Request::METHOD_POST);

        $request->setContent(json_encode($tax_data));

        $client = new \Laminas\Http\Client();
        $options = [
           'adapter'   => 'Laminas\Http\Client\Adapter\Curl',
           'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
           'maxredirects' => 1,
           'timeout' => 30
        ];
        $client->setOptions($options);

        try {
            // Cache the json request in redis cache. Only call Onesource if new request.
            $cacheId = 'tax_request_' . $order['ID'] .'_'  . $request->getContent();

            if (!empty($cResponse = $this->cache->load($cacheId))) {
                $taxResponse = json_decode($cResponse);

                $logMessage = (
                    "\n\n TAX REQUEST IS CACHED \n\n" .
                    "TAX REQUEST PAYLOAD: " .
                    json_encode($tax_data, JSON_PRETTY_PRINT) .
                    "\n\n" .
                    "TAX RESPONSE: " .
                    json_encode($taxResponse, JSON_PRETTY_PRINT) .
                    "\n\n"
                );
                $this->writeToLog($logMessage);

                return ['items' => $tax_items, 'response' => $taxResponse];
            }

            $response = $client->send($request)->getContent();
            $taxResponse = json_decode($response);

            if(!isset($taxResponse->Error)){
                $this->cache->save($response, $cacheId, ["dc_tax_api_cache_tag"], 82000);
            }

            $taxResponse = json_decode($client->send($request)->getContent());
            $logMessage = (
                "\n\n TAX REQUEST IS SENT \n\n" .
                "TAX REQUEST PAYLOAD: " .
                json_encode($tax_data, JSON_PRETTY_PRINT) .
                "\n\n" .
                "TAX RESPONSE: " .
                json_encode($taxResponse, JSON_PRETTY_PRINT) .
                "\n\n"
            );
            $this->writeToLog($logMessage);
        } catch (\Laminas\Http\Exception\RunTimeException $exception) {
            $errorFile = $this->logDirectory . 'taxConnectError.log';
            $logs = json_encode($tax_data, JSON_PRETTY_PRINT);
            $logs .= "ERRORRRRR..........";
            $logs .= "\n\n";
            $logs .= $exception->getMessage() . "\n";
            $logs .= "\n\n";
            file_put_contents($errorFile, $logs, FILE_APPEND);
            return $this;
        }
        return ['items' => $tax_items, 'response' => $taxResponse];
    }


    /**
     * Update Magento fields with calculated tax
     */
    public function updateTax(
        $tax_data,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        if (is_array($tax_data)) {
            $tax_items = $tax_data['items'];
            $tax_response = $tax_data['response'];
            if (isset($tax_response->TotalTaxAmount)) {
                $totalTaxAmount = floatval($tax_response->TotalTaxAmount);
                if (Tax::SAP_CREDIT_AUTH && $totalTaxAmount > 0) {
                    // Increase tax amount to facilitate credit cart authorization
                    // for clents using SAP.
                    $totalTaxAmount += 0.05;
                }
                $total->setTotalAmount('tax', $totalTaxAmount);
                $total->setBaseTotalAmount('tax', $totalTaxAmount);
                $subtotalInclTax
                    = $total->getSubtotal() + $total->getTotalAmount('tax');
                $baseSubtotalInclTax = $total->getBaseSubtotal() +
                    $total->getBaseTotalAmount('tax');
                $total->setSubtotalInclTax($subtotalInclTax);
                $total->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                $total->setBaseSubtotalInclTax($baseSubtotalInclTax);
                try {
                    $shippingAddress = $shippingAssignment->getShipping()->getAddress();
                    $shippingAddress->setBaseSubtotalTotalInclTax($baseSubtotalInclTax);
                    $shippingAddress->setTaxAmount($totalTaxAmount);
                    $shippingAddress->setBaseTaxAmount($totalTaxAmount);
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
            if (isset($tax_response->TaxDetails)) {
                //Transfer line by line tax data to $total
                $taxLines = $tax_response->TaxDetails->Lines;
                if (is_array($taxLines)) {
                    $lines = $taxLines;
                } else {
                    $lines = [$taxLines];
                }
                $current_tax = 0;
                $counter = 0;
                foreach ($lines as $line) {
                    if ($line->LineType == 'PRODUCT') {
                        if ($current_tax > 0) {
                            $item = $tax_items[$counter - 1];
                            $item->setTaxAmount($current_tax);
                            $tax_rate = round(
                                $current_tax / ((float) $item->getPrice()) / $item->getQty(),
                                4
                            );
                            $item->setTaxPercent(floatval($tax_rate) * 100);
                            $current_tax = 0;
                        }
                        $current_tax += floatval($line->TaxAmount);

                        $counter += 1;
                    } else {
                        $current_tax += floatval($line->TaxAmount);
                    }
                }
                if ($current_tax > 0) {
                    $item = $tax_items[$counter - 1];
                    $item->setTaxAmount($current_tax);
                    $tax_rate = round(
                        $current_tax / ((float) $item->getPrice()) / $item->getQty(),
                        4
                    );
                    $item->setTaxPercent(floatval($tax_rate) * 100);
                }
            }
        }
        return $this;
    }

    /**
     * Get the value of configurable items from the Magento admin panel.
     *
     * @access public
     * @param string $name
     * @return value
     */
    public function getConfigValue($name, $storeScope)
    {
        //$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $configPath = 'DSTax_TaxConnect/general/';
        $configName = $configPath . $name;
        return $this->scopeConfig->getValue($configName, $storeScope);
    }

    /**
     * Get tax from OneSource
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function getItemDetails($shippingAssignment, $quote, $shippingQtyCosts) {
        $taxClassId = 0;
        $orderNumber = $quote->getId();
        $customerID = $quote->getCustomerId();
        $firstName = $quote->getCustomerFirstname();
        $lastName = $quote->getCustomerLastname();

        $lineItems = [];
        $items     = $shippingAssignment->getItems();
        $lineNumber = 0;
        if (count($items) > 0) {
            foreach ($items as $item) {
                $lineNumber = $lineNumber + 10;
                $sku                 = $item->getSku();
                $quantity            = $item->getQty();
                $unitPrice           = (float) $item->getPrice();
                $discount            = (float) $item->getDiscountAmount();
                $unitShipping        = (float) $shippingQtyCosts[0]['unit_price'] * $quantity;
                $taxClassId          = $item->getTaxClassId();
                // Don't double tax dynamically priced bundles
                if ($item->getParentItem() != null) {
                    $unitPrice = (float) 0;
                }

                array_push($lineItems, [
                    'line'             => $lineNumber,
                    'sku'              => $sku,
                    'quantity'         => $quantity,
                    'unit_price'       => $unitPrice,
                    'discount'         => $discount,
                    'unit_shipping'    => $unitShipping,
                    'tax_class'        => $taxClassId,
                ]);
            }
        }


        $headerQuote['orderNumber'] = $orderNumber;
        $headerQuote['customerID'] = $customerID;
        if ($firstName === null and $lastName === null) {
            $headerQuote['customerName'] = "GUEST";
        } else{
            $headerQuote['customerName'] = $firstName.' '.$lastName;
        }
        $headerQuote['items'] = $lineItems;

        return $headerQuote;
    }

    /**
     * Get product code
     * @param Tax Class ID
     * @return string
     */
    protected function getProductCode($taxClassId) {
        // Start query - tax class table
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('tax_class'); //gives table name with prefix

        //Select Data from table
        $sql = "Select * FROM " . $tableName;
        $tax_classes_table = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
        // End query

        foreach ($tax_classes_table as $tax_class) {
            if ($tax_class['class_id'] == $taxClassId) {
                return $tax_class['class_name'];
            };
        };

        return null;
    }

    /**
     * Get customer custom fields values
     * @param field_names
     * @return array
     */
    protected function getCustomerCustomFieldsValues($customer_id, $field_names) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        //Select Data from table
        $names = "'" . implode("','", $field_names) . "'";
        $sql = "
            SELECT DISTINCT ea.attribute_code AS `attribute_code`, ce.value AS `value` FROM
                eav_attribute AS ea,
                customer_entity_varchar AS ce
                WHERE ce.entity_id = {$customer_id} AND ea.attribute_code IN ({$names}) AND ea.attribute_id = ce.attribute_id;
        ";
        $rows = $connection->fetchAll($sql);
        $fields_values = [];
        foreach ($rows as $row) {
            $fields_values[$row['attribute_code']] = $row['value'];
        };
        return $fields_values;
    }

    /**
     * Call tax calculation service to get tax details on the quote and items
     *
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @param bool $useBaseCurrency
     * @return \Magento\Tax\Api\Data\TaxDetailsInterface
     */
    protected function getQuoteTaxDetailsInterface($shippingAssignment, $total, $useBaseCurrency)
    {
        $address = $shippingAssignment->getShipping()->getAddress();
        if (!$address->getPostcode()) {
            return;
        }
        //Setup taxable items
        $priceIncludesTax = $this->_config->priceIncludesTax($address->getQuote()->getStore());
        $itemDataObjects  = $this->mapItems($shippingAssignment, $priceIncludesTax, $useBaseCurrency);

        //Add shipping
        $shippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, $useBaseCurrency);
        if ($shippingDataObject != null) {
            # Does shipping also need custom tax? Add protected function here
            # extendShippingItem($shippingDataObject);
            $itemDataObjects[] = $shippingDataObject;
        }

        //process extra taxable items associated only with quote
        $quoteExtraTaxables = $this->mapQuoteExtraTaxables(
            $this->quoteDetailsItemDataObjectFactory,
            $address,
            $useBaseCurrency
        );
        if (!empty($quoteExtraTaxables)) {
            $itemDataObjects = array_merge($itemDataObjects, $quoteExtraTaxables);
        }

        //Preparation for calling taxCalculationService
        $quoteDetails = $this->prepareQuoteDetails($shippingAssignment, $itemDataObjects);

        // $taxDetails = $this->taxCalculationService
        //     ->calculateTax($quoteDetails, $address->getQuote()->getStore()->getStoreId());

        // return $taxDetails;
        return $quoteDetails;
    }

    /**
     * Get tax from OneSource
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function getShippingQtyCosts($baseQuoteTaxDetails) {

        $shippingQtyCosts = [];
        $items     = $baseQuoteTaxDetails->getItems();
        $unitPrice = 0.0;

        //Get total "Product" items count
        $totalNumberOfItems = 0;
        if (count($items) > 0) {
            foreach ($items as $item) {
                if ($item->getType() == 'product') {
                    $totalNumberOfItems     = $totalNumberOfItems + $item->getQuantity();
                }
                if ($item->getType() == 'shipping') {
                    $unitPrice           = (float) $item->getUnitPrice();
                }
            }
        }

        array_push($shippingQtyCosts, [
            'quantity'         => $totalNumberOfItems,
            'unit_price'       => $unitPrice,
        ]);
        // echo '<pre>'; print_r($shippingQtyCosts[0]['unit_price']); echo '</pre>';

        return $shippingQtyCosts;
    }

   /**
     * For the USA, return the first 5 digits in the zipcode.
     * For everyone else, return the entire postalcode.
     * @param $country
     * @param $postalcode
     */
    private function getZipcode($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && strlen($postalcode) > 5) {
                return substr($postalcode, 0, 5);
            }
        }
        return $postalcode;
    }

    /**
     * For the USA, return the last 4 digits of the zipcode.
     * There may or may not be a dash in the code.
     * For everyone else, the zip extension nis not used.
     * @param $country
     * @param $postalcode
     * @return string
     */
    private function getZipExtension($country, $postalcode) {
        if ($country == "USA" || $country == "US") {
            if ($postalcode && (strlen($postalcode) == 9 || strlen($postalcode) == 10)) {
                return substr($postalcode, -4);
            }
        }
        return null;
    }

    /**
     * Add user attribute to $soap_item here instead of repeating this for
     * FREIGHT and DISCOUNT lines.
     *
     * @param array $soap_item item object
     *
     * @return array
     */
    protected function userAttributes($soap_item)
    {
        $userAttributes = [];
        // For all Tivo items pass product_code to attribute 2
        if (Tax::USE_COMMODITY_CODE) {
            $userAttributes['2'] = $soap_item['product_code'];
        }
        // For Tivo items excluding FREIGHT and DISCOUNT check if it's in a bundle
        if (Tax::TIVO_BUNDLE_ATTRIBUTE) {
            if ($soap_item['in_bundle']) {
                // Tivo bundle user attribute
                $userAttributes['3'] = 'B';
            }
        }
        return $userAttributes;
    }
}
